// Setup code
// ------------------------------------------------------------------------------
var express = require('express');
var app = express();
app.set('port', process.env.PORT || 3000);

var handlebars = require('express-handlebars');
app.engine('handlebars', handlebars({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));


// ------------------------------------------------------------------------------

app.get(['/','/logout'], function (req, res) {
    res.render('home');
});

app.get('/signup', function (req, res) {
    res.render('signup');
});

app.get('/login', function (req, res) {
    res.render('login');
});

app.get('/addPost', function (req, res) {
    res.render('addPost');
});


// Serve files form "/public" folder
app.use(express.static(__dirname + "/public"));

// --------------------------------------------------------------------------

// Start the server running.
app.listen(app.get('port'), function () {
    console.log('Express started on http://localhost:' + app.get('port'));
});